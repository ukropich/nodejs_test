import {Controller, Delete, Get, Inject, NotFoundException, Param} from '@nestjs/common';
import {UserServiceInterface} from '../client/user-service.interface';
import {AvatarStorageInterface} from '../storage/avatar-storage.interface';

@Controller('/api/user')
export class UserController {

    constructor(
        @Inject('UserServiceInterface') private readonly userService: UserServiceInterface,
        @Inject('AvatarStorageInterface') private readonly avatarStorage: AvatarStorageInterface
    ) {}

    @Get('/:userId')
    public async getUser(@Param('userId') userId: string): Promise<any> {
        try {
            return await this.userService.getUserInfo(userId);
        } catch (e){
            let message = 'Something went wrong';

            if (e.response !== undefined && e.response.status === 404) {
                message = 'User not found';
            }

            return {message};
        }
    }

    @Get('/:userId/avatar')
    public async getAvatar(@Param('userId') userId: string): Promise<any> {
        try {
            return await this.userService.getUserAvatar(userId);
        } catch (e){
            let message = 'Something went wrong';

            if (e.response !== undefined && e.response.status === 404) {
                message = 'User not found';
            }

            return {message};
        }
    }

    @Delete('/:userId/avatar')
    public async deleteAvatar(@Param('userId') userId: string): Promise<object> {
        await this.avatarStorage.remove(userId);

        return {

        };
    }
}
