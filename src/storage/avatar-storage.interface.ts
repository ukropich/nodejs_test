export interface AvatarStorageInterface {
    has(userId: string): boolean;
    get(userId: string): string;
    save(userId: string, base64Image: string): void;
    remove(userId: string): void;
}