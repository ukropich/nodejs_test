import {AvatarStorageInterface} from './avatar-storage.interface';

export class InMemoryStorage implements AvatarStorageInterface {
    constructor() {  }
    private storage = {};

    has(userId: string): boolean {
        return !!this.storage[userId];
    }

    get(userId: string): string {
        return this.has(userId) ? this.storage[userId] : null;
    }

    save(userId: string, base64Image: string): void {
        this.storage[userId] = base64Image;
    }

    remove(userId: string): void {
        delete this.storage[userId];
    }
}