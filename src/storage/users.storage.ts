import * as fs from 'fs';
import {UsersStorageInterface} from './users-storage.interface';
import {UserInterface} from '../models/user.interface';
import {join} from 'path';
import {Injectable} from '@nestjs/common';

@Injectable()
export class UsersStorage implements UsersStorageInterface {
    private jsonFilename = 'users.json';

    constructor(private readonly storagePath: string) {};

    appendUsers(users: UserInterface[]): void {
        let storedUsers = this.getAllUsers();
        this.storeUsers(storedUsers.concat(users));
    }

    private getAllUsers(): UserInterface[] {
        const filePath = join(this.storagePath, this.jsonFilename);
        try {
            if (fs.existsSync(filePath)) {
                return JSON.parse(fs.readFileSync(filePath).toString());
            }
        } catch (e) { console.error(e) }

        return [];
    }

    private storeUsers(users: UserInterface[]) {
        const filePath = join(this.storagePath, this.jsonFilename);

        try{
            fs.writeFileSync(filePath, JSON.stringify(users));
        }catch (e) {
            console.error(e);
        }

    }
}
