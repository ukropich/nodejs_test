import {AvatarStorageInterface} from './avatar-storage.interface';
import * as fs from 'fs';
import * as path from 'path';
import {Injectable} from '@nestjs/common';

@Injectable()
export class LocalFileSystemStorage implements AvatarStorageInterface {

    constructor(private readonly storageDir:string){};

    has(userId: string): boolean {
        return fs.existsSync(this.getPathForUserAvatar(userId));
    }

    get(userId: string): string {
        try {
            return fs.readFileSync(this.getPathForUserAvatar(userId)).toString();
        } catch {
            return null;
        }
    }

    save(userId: string, base64Image: string): void {
        return fs.writeFileSync(this.getPathForUserAvatar(userId), base64Image);
    }

    remove(userId: string): void {
        return fs.unlinkSync(this.getPathForUserAvatar(userId));
    }

    private getPathForUserAvatar(userId: string)
    {
        return path.join(this.storageDir, userId);
    }
}
