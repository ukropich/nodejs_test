import {UserInterface} from '../models/user.interface';

export interface UsersStorageInterface {
    appendUsers(users:UserInterface[]): void;
}