import * as dotenv from 'dotenv';

dotenv.config();
const env = process.env;

export const config: any = {
    storageDir: env.STORAGE_DIR
};