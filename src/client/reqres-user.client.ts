import {UserServiceInterface} from './user-service.interface';
import {UserInterface} from '../models/user.interface';
import {HttpService, Inject, Injectable} from '@nestjs/common';
import {AvatarStorageInterface} from '../storage/avatar-storage.interface';

@Injectable()
export class ReqresUserClient implements UserServiceInterface {

    public static readonly serviceHost = 'https://reqres.in';
    public static readonly userInfoEndpoint = '/api/users/{userId}';
    public static readonly usersListEndpoint = '/api/users?page={page}';

    constructor(private readonly httpService: HttpService, @Inject('AvatarStorageInterface') private readonly avatarStorage: AvatarStorageInterface) {}

    async getUserInfo(userId): Promise<UserInterface> {

        let userEndpoint = ReqresUserClient.userInfoEndpoint.replace('{userId}', userId);
        let response = await this.httpService.get(ReqresUserClient.serviceHost + userEndpoint).toPromise();

        return response.data.data;

    }

    async getUserAvatar(userId: string): Promise<any> {
        let avatarBase64 = null;
        if (this.avatarStorage.has(userId)) {
            avatarBase64 = this.avatarStorage.get(userId);
        }

        if (!avatarBase64) {
            let userInfo = await this.getUserInfo(userId);

            let avatar = await this.httpService.get(userInfo.avatar).toPromise();
            avatarBase64 = Buffer.from(avatar.data).toString('base64');

            this.avatarStorage.save(userId, avatarBase64);
        }

        return avatarBase64;
    }

    async getUsersList(pageNumber: string): Promise<UserInterface[]> {
        let usersListEndpoint = ReqresUserClient.usersListEndpoint.replace('{page}', pageNumber);
        try {
            let response = await this.httpService.get(ReqresUserClient.serviceHost + usersListEndpoint).toPromise()

            return response.data.data;
        } catch (e) {
            console.log(e);

            return [];
        }
    }
}