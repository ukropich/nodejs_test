import {UserInterface} from '../models/user.interface';

export interface UserServiceInterface {
    getUserInfo(userId: string): Promise<UserInterface>;
    getUserAvatar(userId: string): Promise<any>;
    getUsersList(pageNumber: string);
}