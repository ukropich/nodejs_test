import {HttpModule, Module} from '@nestjs/common';
import {UserController} from './controllers/user.controller';
import {UserServiceInterface} from './client/user-service.interface';
import {ReqresUserClient} from './client/reqres-user.client';
import {AvatarStorageInterface} from './storage/avatar-storage.interface';
import {LocalFileSystemStorage} from './storage/local-file-system.storage';
import {config} from './config/config';
import {ScheduleModule} from 'nest-schedule';
import {UsersListFetcherService} from './cron/users-list-fetcher-service';
import {UsersStorage} from './storage/users.storage';


@Module({
    imports: [HttpModule, ScheduleModule.register()],
    controllers: [UserController],
    providers: [
        UsersListFetcherService,
        {provide: 'UserServiceInterface', useClass: ReqresUserClient},
        {provide: 'AvatarStorageInterface', useValue: new LocalFileSystemStorage(config.storageDir)},
        {provide: 'UsersStorageInterface', useValue: new UsersStorage(config.storageDir)},
        {provide: 'STORAGE_DIRECTORY', useValue: config.storageDir},
    ]
})
export class AppModule {

}
