import {ReqresUserClient} from '../../client/reqres-user.client';
import {InMemoryStorage} from '../../storage/in-memory.storage';
import {HttpServiceMock} from '../../__mocks__/http.service.mock';

describe('ReqresUserClient', () => {

    describe('getUserAvatar', () => {

        it('should return avatar from request if it was not stored', async () => {
            const storage = new InMemoryStorage();
            const userId = 1;
            const fileContent = 'test file content';

            const httpService = new HttpServiceMock();
            const userInfoUrl = ReqresUserClient.serviceHost + ReqresUserClient.userInfoEndpoint.replace('{userId}', userId.toString());
            const urls = {};
            urls[userInfoUrl] = {data:{avatar: 'avatar_address'}};
            urls['avatar_address'] = fileContent;

            httpService.__setUrls(urls);

            let client = new ReqresUserClient(httpService, storage);

            const avatar = await client.getUserAvatar(userId.toString());

            expect(avatar).toEqual(Buffer.from(fileContent).toString('base64'));

        });

        it('should return avatar from storage if it\'s exists', async () => {
            const storage = new InMemoryStorage();
            const userId = 1;
            const fileContent = 'test file content';
            storage.save(userId.toString(), fileContent);

            let client = new ReqresUserClient(new HttpServiceMock(), storage);

            const avatar = await client.getUserAvatar(userId.toString());

            expect(avatar).toEqual(fileContent);

        });


    });

});