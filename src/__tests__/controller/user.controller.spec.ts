import * as fs from 'fs';
import { Test, TestingModule } from '@nestjs/testing';
import {UserController} from '../../controllers/user.controller';
import {HttpModule, HttpService} from '@nestjs/common';
import {ReqresUserClient} from '../../client/reqres-user.client';
import {LocalFileSystemStorage} from '../../storage/local-file-system.storage';
import {join} from 'path';

describe('UserController', () => {
    let app: TestingModule;

    const response = { send: (body?: any) => { }, render: () => {} };
    const storageDir = '/tmp';

    beforeAll(async () => {
        app = await Test.createTestingModule({
            controllers: [UserController, HttpModule],
            providers: [
                {provide: 'UserServiceInterface', useValue: new ReqresUserClient(new HttpService(), new LocalFileSystemStorage(storageDir))},
                {provide: 'AvatarStorageInterface', useValue: new LocalFileSystemStorage(storageDir)},
            ],
        }).compile();
    });

    describe('getUser', () => {
        it('should return user info by id', async () => {
            const userId = 1;
            const userInfoEndpoint = 'https://reqres.in/api/users/' + userId.toString();

            const http = new HttpService();
            let responseFromServer = http.get(userInfoEndpoint).toPromise();

            const userController = app.get<UserController>(UserController);
            const userInfoFromController = await userController.getUser(userId.toString());

            const userInfo = await responseFromServer;
            expect(userInfoFromController).toEqual(userInfo.data.data);
        });
    });

    describe('getAvatar', () => {
        it('should return user\'s avatar', async () => {
            const userId = 1;
            const http = new HttpService();

            const userController = app.get<UserController>(UserController);
            const userInfoFromController = await userController.getUser(userId.toString());
            let avatarResponseFromServer = http.get(userInfoFromController.avatar).toPromise();

            const avatarFromController = await userController.getAvatar(userId.toString());
            const avatarFromServer = await avatarResponseFromServer;
            expect(avatarFromController).toEqual(Buffer.from(avatarFromServer.data).toString('base64'));
        });

        it('should store avatar', async () => {
            const userId = 1;
            const http = new HttpService();

            const userController = app.get<UserController>(UserController);
            const userInfoFromController = await userController.getUser(userId.toString());
            let avatarResponseFromServer = http.get(userInfoFromController.avatar).toPromise();

            await userController.getAvatar(userId.toString());
            const avatarFromServer = await avatarResponseFromServer;
            expect(
                fs.readFileSync(join(storageDir, userId.toString())).toString()
            ).toEqual(Buffer.from(avatarFromServer.data).toString('base64'));
        });
    });

    describe('deleteAvatar', () => {
        it('should delete user\'s avatar', async () => {
            const userId = 1;
            const userController = app.get<UserController>(UserController);

            await userController.getAvatar(userId.toString());
            expect(fs.existsSync(join(storageDir, userId.toString()))).toBeTruthy();

            await userController.deleteAvatar(userId.toString());

            expect(fs.existsSync(join(storageDir, userId.toString()))).toBeFalsy();
        });

    });

});