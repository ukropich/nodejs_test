import {join} from 'path';
import {LocalFileSystemStorage} from '../../storage/local-file-system.storage';

jest.mock('fs');

describe('LocalFileSystemStorage', () => {
    const STORAGE_DIR = '/test/storage/';

    describe('has', () => {
        it('should return true if user avatar exists', () => {

            let usersWithAvatars = [2, 3, 5];

            const mockFiles = {};
            for (let userId of usersWithAvatars) {
                mockFiles[join(STORAGE_DIR, userId.toString())] = 'Base64 content of avatar';
            }

            require('fs').__setMockFiles(mockFiles);

            let localStorage = new LocalFileSystemStorage(STORAGE_DIR);

            for (let userId of usersWithAvatars) {
                expect(localStorage.has(userId.toString())).toBeTruthy();
            }
        });

        it('should return false if user avatar not exists', () => {

            let usersWithAvatars = [2];

            let usersWithoutSavedAvatars = [3, 4, 5]

            const mockFiles = {};
            for (let userId of usersWithAvatars) {
                mockFiles[join(STORAGE_DIR, userId.toString())] = 'Base64 content of avatar';
            }

            require('fs').__setMockFiles(mockFiles);

            let localStorage = new LocalFileSystemStorage(STORAGE_DIR);

            for (let userId of usersWithoutSavedAvatars) {
                expect(localStorage.has(userId.toString())).toBeFalsy();
            }
        });
    });

    describe('get', () => {
        it('should return file content if file exists', () => {
            let userId = 2;
            let fileContent = 'Base64 content of avatar'
            const mockFiles = {};
            mockFiles[join(STORAGE_DIR, userId.toString())] = fileContent;

            require('fs').__setMockFiles(mockFiles);

            let localStorage = new LocalFileSystemStorage(STORAGE_DIR);
            expect(localStorage.get(userId.toString())).toEqual(fileContent);
        });

        it('should return null if file not found', () => {
            let userId = 2;
            const mockFiles = {};

            require('fs').__setMockFiles(mockFiles);

            let localStorage = new LocalFileSystemStorage(STORAGE_DIR);
            expect(localStorage.get(userId.toString())).toBeNull();
        });

    });

});