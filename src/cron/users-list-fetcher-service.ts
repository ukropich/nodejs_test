import * as fs from 'fs';
import {Cron, NestSchedule} from 'nest-schedule';
import {Inject, Injectable} from '@nestjs/common';
import {UserServiceInterface} from '../client/user-service.interface';
import {join} from 'path';
import {UsersStorageInterface} from '../storage/users-storage.interface';

@Injectable()
export class UsersListFetcherService extends NestSchedule {

    private readonly lastFetchedPageFilename = '__last-fetched-users-list';

    constructor(
        @Inject('UserServiceInterface') private readonly userClient: UserServiceInterface,
        @Inject('UsersStorageInterface') private readonly userStorage: UsersStorageInterface,
        @Inject('STORAGE_DIRECTORY') private readonly storageDir: string,
    ){
        super();
    }

    @Cron('*/1 * * * *', {
        startTime: new Date(),
        endTime: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
    })
    async cronJob(): Promise<void> {
        console.log('Started fetching users');
        let lastFetchedPage = this.getLastFetchedPageNumber();
        lastFetchedPage++;
        let users = await this.userClient.getUsersList(lastFetchedPage.toString());
        if (users.length > 0) {
            this.userStorage.appendUsers(users);
            this.saveLastFetchedPageNumber(lastFetchedPage);
        }
        console.log('Users fetching was finished');
    }

    private getLastFetchedPageNumber(): number {
        try {
            let filename = join(this.storageDir, this.lastFetchedPageFilename);

            return parseInt(fs.readFileSync(filename).toString());
        } catch (e) {
            return 0;
        }
    }

    private saveLastFetchedPageNumber(lastFetchedPage: number) {
        try {
            let filename = join(this.storageDir, this.lastFetchedPageFilename);
            return fs.writeFileSync(filename, lastFetchedPage);
        } catch (e) {
            console.error(e);
        }
    }
}