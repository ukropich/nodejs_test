import {HttpService, NotFoundException} from '@nestjs/common';
import {Observable} from 'rxjs/index';
import { of } from 'rxjs';
import { AxiosResponse } from 'axios'


export class HttpServiceMock extends HttpService{
    private urls = {};

    public __setUrls(newUrls) {
        this.urls = newUrls;
    }

    public get<T = any>(url: string): Observable<any>{

        if (this.urls[url] !== undefined) {
            return of({
                data: this.urls[url]
            });
        }

        throw new NotFoundException('Not Found');
    }
}

