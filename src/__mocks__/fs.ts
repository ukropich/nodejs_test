const fs:any = jest.genMockFromModule('fs');

let mockFiles = {};
function __setMockFiles(newMockFiles) {
    mockFiles = newMockFiles;
}

function existsSync(path){
    return mockFiles[path] !== undefined;
}

function readFileSync(path) {
    if (mockFiles[path] !== undefined) {
        return mockFiles[path];
    }

    throw new Error();
}

function writeFileSync(path, content) {
    return mockFiles[path] = content;
}

function unlinkSync(path) {
    delete mockFiles[path];
}

fs.__setMockFiles = __setMockFiles;
fs.existsSync = existsSync;
fs.readFileSync = readFileSync;
fs.writeFileSync = writeFileSync;
fs.unlinkSync = unlinkSync;

module.exports = fs;