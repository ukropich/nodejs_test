FROM node:latest

# Create and define the node_modules's cache directory.
RUN mkdir /usr/src/cache
WORKDIR /usr/src/cache
COPY package.json ./
COPY package-lock.json ./

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN npm install -g nodemon
RUN npm install -g typescript
RUN npm install -g ts-node
RUN npm install
ADD . .

# Create and define the application's working directory.
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

EXPOSE 3000
CMD ["npm", "start"]
